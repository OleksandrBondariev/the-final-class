<?php

/**
 * Класс init
 * 
 * Финальный класс 
 * 
 * {@link __construct($count)}      конструктор класса, создает соединение с БД,
 *                                  вызывает методы класса
 *                                  create($mysqli) и fill($mysqli, $count)
 *                                  Параметры: 
 *                                      $count - тип int, кол-во полей в таблице,
 *                                      замолняемой методом fill($mysqli, $count)
 * 
 * 
 * {@link create($mysqli)}          создает таблицу 'test' в базе данных,
 *                                  доступен только для методов класса
 *                                  Параметры: 
 *                                      $mysqli - соединение с БД 
 * 
 * {@link fill($mysqli, $count)}    заполняет 'test' случайными данными,
 *                                  доступен только для методов класса
 *                                  Параметры: 
 *                                      $mysqli - соединение с БД 
 *                                      $count - кол-во полей таблицы (int)
 * 
 * 
 * {@link get()}                    выбирает из таблицы test, данные по критерию:
 *                                  result среди значений 'normal' и 'success'
 *                                  оступен извне класса
 *                                      @return array
 * @final
 */
final class init {

    /**
     * Конструктор
     * 
     * Создает соединение с базаой MySQL, вызов методов 
     * create($mysqli),
     * fill($mysqli, $count)
     * 
     * @param int
     */
    public function __construct($count) {
        /**
         *  Устанавливаем новое соединение с сервером MySQL
         */
        $mysqli = new mysqli("localhost", "root", "9338922", "lv2_1");
        $this->create($mysqli);
        $this->fill($mysqli, $count);
    }

    /**
     * Метод класса
     * 
     * Создает таблицу test, содержащую 5 полей:
     *           id             целое, автоинкрементарное
     *           script_name    строковое, длиной 25 символов
     *           start_time     целое
     *           end_time	целое
     *           result         один вариант из 'normal', 'illegal', 'failed', 'success'

     * 
     * @access private
     * @param object mysqli
     */
    private function create($mysqli) {

        $create = "CREATE TABLE IF NOT EXISTS `test` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `script_name` varchar(25) NOT NULL DEFAULT '0',
        `start_time` int(11) NOT NULL DEFAULT '0',
        `end_time` int(11) NOT NULL DEFAULT '0',
        `result` enum('normal','illegal','failed','success') NOT NULL,
         PRIMARY KEY (`id`)
        )";

        /**
         * Выполняем подготовленный запрос к базе данных
         */
        $mysqli->query($create);
    }

    /**
     * Метод класса
     * 
     * Заполняет таблицы test случайными данными
     * 
     * @access private
     * @param object mysqli, int count
     */
    private function fill($mysqli, $count) {

        /**
         * Подготавливаем SQL выражение к выполнению
         */
        $stmt = $mysqli->prepare("INSERT INTO test (script_name, start_time, end_time, result) VALUES (?, ?, ?,?)");

        /**
         * Привязка переменных к параметрам подготавливаемого запроса
         *
         * Первый парамметр указывает тип значения привязываемой переменной
         * s - string
         * i - int
         */
        $stmt->bind_param('siis', $scriptName, $startTime, $endTime, $result);


        $resultsArray = array('normal', 'illegal', 'failed', 'success');
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        /**
         *  Заполняем таблицу
         * 
         * В цикле заполняем случайными значениями привязаные  переменные
         *  и сохраняем в базе данных
         */
        for ($i = 0; $i < $count; $i++) {

            $randomString = '';
            for ($y = 0; $y < 5; $y++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $scriptName = $randomString;
            $startTime = rand();
            $endTime = rand();
            $result = $resultsArray[rand(0, 3)];

            /* Выполняет подготовленный запрос */
            $stmt->execute();
        }
        /* закрытие соединения */
        $stmt->close();
    }

    /**
     * Метод класса
     * 
     * Выбирает из таблицы test, данные по критерию: result среди значений
     * 'normal' и 'success' и сохраняет их в массив
     * 
     * @access public
     * @return array
     */
    public function get() {

        /**
         *  Устанавливаем новое соединение с сервером MySQL
         */
        $mysqli = new mysqli("localhost", "root", "9338922", "lv2_1");
        /**
         * Выполняем подготовленный запрос к базе данных
         */
        $query = "SELECT * FROM test WHERE result not like '%il%'";

        if ($result = $mysqli->query($query)) {

            $rowArray = array();

            /**
             * Извлечение результирующего ряда в виде ассоциативного массива
             */
            while ($row = $result->fetch_assoc()) {
                $rowArray[] = $row;
            }
            /* удаление выборки */
            $result->free();
        }
        /* закрытие соединения */
        $mysqli->close();

        return $rowArray;
    }

}
