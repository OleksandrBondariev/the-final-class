<?php

/**
 * Определяем пользовотельский обработчик ошибок
 */
function my_error_handler($errno, $errstr, $errfile, $errline) {
    var_dump("Hello my_error_handler");
    print "<pre>\n<b>";

    /**
     * Тип ошибки
     * 
     * Определяем и выводим тип ошибки
     */
    switch ($errno) {
        /**
         * Тип ошибки E_ERROR (integer)
         * 
         * Фатальные ошибки времени выполнения. 
         * Выполнение скрипта в таком случае прекращается.
         */
        case E_ERROR: print "Error";
            break;
        /**
         * Тип ошибки E_WARNING(integer)
         * 
         * Предупреждения времени выполнения (не фатальные ошибки).
         */
        case E_WARNING: print "Warning";
            break;
        /**
         * Тип ошибки E_PARSE (integer)
         * 
         * Ошибки на этапе компиляции
         */
        case E_PARSE: print "Parse Error";
            break;
        /**
         * Тип ошибки E_NOTICE (integer)
         * 
         * Уведомления времени выполнения. Указывают на то, что
         *  во время выполнения скрипта произошло что-то,
         *  что может указывать на ошибку, это может происходить
         *  и при обычном выполнении программы.
         */
        case E_NOTICE: print "Notice";
            break;
        /**
         * Тип ошибки E_CORE_ERROR (integer)
         * 
         * Фатальные ошибки, которые происходят во время запуска РНР. 
         * Такие ошибки схожи с E_ERROR, за исключением того, 
         * что они генерируются ядром PHP.
         */
        case E_CORE_ERROR: print "Core Error";
            break;
        /**
         * Тип ошибки E_CORE_WARNING (integer)
         * 
         * Предупреждения (не фатальные ошибки), которые происходят во время 
         * начального запуска РНР. Такие предупреждения схожи с E_WARNING, 
         * за исключением того, что они генерируются ядром PHP
         */
        case E_CORE_WARNING: print "Core Warning";
            break;

        /**
         * Тип ошибки E_STRICT(integer)
         * 
         * Включаются для того, чтобы PHP предлагал изменения в коде, 
         * которые обеспечат лучшее взаимодействие и совместимость кода
         */
        case E_STRICT: print "Strict Notice";
            break;
        /**
         * Тип ошибки E_RECOVERABLE_ERROR(integer)
         * 
         * Фатальные ошибки с возможностью обработки. Такие ошибки указывают, 
         * что, вероятно, возникла опасная ситуация, но при этом, скриптовый д
         * вижок остается в стабильном состоянии.
         */
        case E_RECOVERABLE_ERROR: print "Recoverable Error";
            break;
        /**
         * Необработаная ошибка
         * 
         */
        default: print "Unknown error ($errno)";
            break;
    }
    print ":</b> <i>$errstr</i> in <b>$errfile</b> on line <b>$errline</b>\n";
    if (function_exists('debug_backtrace')) {
        print "backtrace:\n";
        /**
         * Вывод стека вызовов функций в массив
         * 
         * @return  (array)Возвращает массив вложенных ассоциативных массивов 
         */
        $backtrace = debug_backtrace();

        /**
         * Извлекаем первый элемент массива
         */
        array_shift($backtrace);
        /**
         * Вывод информации об ошибке
         */
        foreach ($backtrace as $i => $l) {
            print "[$i] in function <b>{$l['class']}{$l['type']}{$l['function']}</b>";
            if ($l['file'])
                print " in <b>{$l['file']}</b>";
            if ($l['line'])
                print " on line <b>{$l['line']}</b>";
            print "\n";
        }
    }
}

/**
 * Информация о фатальной ошибке
 * 
 * 
 * Если произошла фатальная ошибка, выводим инфомацию о ней.
 * 
 */
function dbg_last_error() {
    $e = error_get_last();
    if (($e['type'] & E_COMPILE_ERROR) || ($e['type'] & E_ERROR) ||
            ($e['type'] & E_CORE_ERROR) || ($e['type'] & E_RECOVERABLE_ERROR)) {

        print "FATAL ERROR: {$e['message']}</b>";
        if ($e['file'])
            print " in <b>{$e['file']}</b>";
        if ($e['line'])
            print " on line <b>{$e['line']}</b>";
        print "\n";
    }
}
